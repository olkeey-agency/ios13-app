//
//  TabBar.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 20/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

struct TabBar: View {
    var body: some View {
        TabView {
            Home().tabItem {
                Image(systemName: "music.house")
                Text("Home")
            }
            
            ListDisplay().tabItem {
                Image(systemName: "rectangle.stack")
                Text("Tickets")
            }
        }
        .edgesIgnoringSafeArea(.top)
    }
}

struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
        TabBar().environment(\.colorScheme, .dark)
    }
}

