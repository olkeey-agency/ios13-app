//
//  ContentfulAPI.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 26/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI
import Contentful

let client = Client(spaceId: "v5j72rq1pnzq", accessToken: "a9BgQTnR4sFwXbLtqVY0EHQ8MgREhDlGH9_EP9pQGlw")

func getArray(id: String, completion: @escaping([Entry]) -> ()) {
    let query = Query.where(contentTypeId: id)
    
    client.fetchArray(of: Entry.self, matching: query) { result in
        switch result {
        case .success(let array):
            DispatchQueue.main.async {
                completion(array.items)
            }
        case .error(let error):
            print(error)
        }
    }
}

class ContentStore: ObservableObject {
    @Published var items: [ItemDetails] = itemAsPlatData
    
    init() {
        getArray(id: "signataryPlatform") { (items) in
            items.forEach { (item) in
                self.items.append(ItemDetails(
                    title: item.fields["title"] as! String,
                    subtitle: item.fields["subtitle"] as! String,
                    image: item.fields.linkedAsset(at: "image")?.url ?? URL(string: "")!,
                    logo: #imageLiteral(resourceName: "Logo1"),
                    color: #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),
                    show: false))
            }
        }
    }
}
