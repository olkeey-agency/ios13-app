
//
//  DataStore.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 26/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI
import Combine

class DataStore: ObservableObject {
    @Published var posts: [Post] = []
    
    init() {
        getPosts()
    }
    
    func getPosts() {
        Api().getPosts { (posts) in
            self.posts = posts
        }
    }
}
