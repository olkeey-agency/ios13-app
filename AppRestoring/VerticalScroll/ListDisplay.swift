//
//  DetailedDisplay.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 22/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct ListDisplay: View {
//    @State var show = false
//    @State var show2 = false
//    @State var items = itemAsPlatData
    @ObservedObject var store = ContentStore()
    @State var activeItem = false
    @State var activeIndex = -1
    @State var activeView = CGSize.zero
    
    var body: some View {
        ZStack {
            Color.black.opacity(activeItem ? 0.3 : 0)
                .animation(.linear)
                .edgesIgnoringSafeArea(.all)
            ScrollView(showsIndicators: false) {
                VStack(spacing: 30.0) {
                    Text("Available")
                        .font(.largeTitle)
                        .bold()
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal, 30)
                        .padding(.top, 30)
                        .blur(radius: activeItem ? 20 : 0)
                    
                    ForEach(store.items.indices, id: \.self) { idx in
                        GeometryReader { geometry in
                            ListItem(
                                show: self.$store.items[idx].show,
                                item: self.store.items[idx],
                                index: idx,
                                activeIdx: self.$activeIndex,
                                active: self.$activeItem,
                                activeView: self.$activeView
                            )
                                .offset(y: self.store.items[idx].show ? -geometry.frame(in: .global).minY : 0)
                                .opacity(self.activeIndex != idx && self.activeItem ? 0 : 1)
                                .scaleEffect(self.activeIndex != idx && self.activeItem ? 0.5 : 1)
                                .offset(x: self.activeIndex != idx && self.activeItem ? screen.width : 0)
                        }
                        .frame(height: 280)
                        .frame(maxWidth: self.store.items[idx].show ? .infinity : screen.width - 60)
                        .zIndex(self.store.items[idx].show ? 1 : 0)
                    }
                    
                }
                .frame(width: screen.width)
                .animation(.spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0))
            }
            .statusBar(hidden: self.activeItem)
        }
    }
}

struct ListDisplay_Previews: PreviewProvider {
    static var previews: some View {
        ListDisplay()
    }
}

struct ListItem: View {
    @Binding var show: Bool
    var item: ItemDetails
    var index: Int
    @Binding var activeIdx: Int
    @Binding var active: Bool
    @Binding var activeView: CGSize
    
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack(alignment: .leading, spacing: 30.0) {
                Text("Take your SwiftUI app to the app Store with advanced techiniques like API data, packages and CMS")
                
                Text("About this App.")
                    .font(.title)
                    .bold()
                
                Text("I Thought i Would live just for 100 years, but being alive for 500 years has  been a pleasure, i`ve met so many incredible humans. The hardest part was to keep the memory of the ones from the start. The human memory is not the great, therefore, many of the old memories and persons from the early days, simply vanished, sometimes i look at pictures from that time - pictures that are very rare for me - and i just can`t believe that i`ve been to some places, some eras, and with some people that clearly were very intimate to me.")
                
                Text("I Thought i Would live just for 100 years, but being alive for 500 years has  been a pleasure, i`ve met so many incredible humans. The hardest part was to keep the memory of the ones from the start. The human memory is not the great, therefore, many of the old memories and persons from the early days, simply vanished, sometimes i look at pictures from that time - pictures that are very rare for me - and i just can`t believe that i`ve been to some places, some eras, and with some people that clearly were very intimate to me.")
            }
            .padding(30)
            .frame(maxWidth: show ? .infinity : screen.width - 60, maxHeight: show ? .infinity : 280, alignment: .top)
            .offset(y: show ? screen.height / 2 : 0 )
            .background(Color("background2"))
            .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
            .shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 20)
            .opacity(show ? 1 : 0)
            
            VStack {
                HStack(alignment: .top) {
                    VStack(alignment: .leading, spacing: 8.0) {
                        Text(item.title)
                            .font(.system(size: 24, weight: .bold))
                            .foregroundColor(Color(item.colorText))
                        Text(item.subtitle)
                            .foregroundColor(Color.white.opacity(0.7))
                    }
                    Spacer()
                    ZStack {
                        Image(uiImage: item.logo)
                            .opacity(show ? 0 : 1)
                        VStack {
                            Image(systemName: "xmark")
                            
                                .foregroundColor(.white)
                                .font(.system(size: 16, weight: .semibold))
                        }
                            .frame(width: 36, height: 36)
                            .background(Color.black)
                            .clipShape(Circle())
                            .opacity(show ? 1 : 0)
                        
                        
                    }
                }
                Spacer()
                WebImage(url: item.image)
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: .infinity)
                    .frame(height: 140, alignment: .top)
            }
            .padding(show ? 30 : 20)
            .padding(.top, show ? 30 : 0)
            .frame(maxWidth: show ? .infinity : screen.width - 60, maxHeight: show ? screen.height / 2 : 280)
            .background(Color(item.color))
            .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
            .shadow(color: Color(item.color).opacity(0.3), radius: 10, x: 0, y: 10)
            .animation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0))
            .gesture(
                show ? DragGesture().onChanged { value in
                    guard value.translation.height < 300 else {return}
                    guard value.translation.height > 0 else { return }
                        self.activeView = value.translation
                }
            .onEnded { value in
                if self.activeView.height > 50 {
                    self.show = false
                    self.active = false
                    self.activeIdx = -1
                }
                self.activeView  = .zero
            }
                : nil
            )
            .onTapGesture {
                self.show = true
                self.active.toggle()
                if self.show {
                    self.activeIdx = self.index
                }
                else {
                    self.activeIdx = -1
                }
            }
            if show {
                itemDetailsCompleteView(item: item, active: $active, activeIdx: $activeIdx, show: $show)
                    .background(Color("background2").zIndex(0).cornerRadius(30))
                    .animation(nil)
                   
            }
        }
        .frame(height: show ? screen.height : 280)
        .scaleEffect(1 - self.activeView.height / 1000)
        .rotation3DEffect(Angle(degrees: Double(self.activeView.height / 10)), axis: (x:0, y: 10.0, z: 0))
        .animation(.spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0))
        .gesture(
            self.show ? DragGesture().onChanged { value in
                guard value.translation.height < 300 else {return}
                guard value.translation.height > 0 else { return }
                    self.activeView = value.translation
            }
        .onEnded { value in
            if self.activeView.height > 150 {
                self.show = false
                self.active = false
                self.activeIdx = -1
            }
            self.activeView  = .zero
        }
            : nil
        )
        .edgesIgnoringSafeArea(.all)
    }
}


var itemAsPlatData = [ ItemDetails(title: "Virtual Democracies", subtitle: "Associação de Usuários Livres", image: URL(string: "https://dl.dropbox.com/s/pmggyp7j64nvvg8/Certificate%402x.png?dl=0")! , logo: #imageLiteral(resourceName: "Logo1"), color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), colorText: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),show: false), ItemDetails(title: "Microappollis", subtitle: "Microapplications for Everyone", image: URL(string: "https://dl.dropbox.com/s/pmggyp7j64nvvg8/Certificate%402x.png?dl=0")! , logo: #imageLiteral(resourceName: "Logo2"), color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), show: false), ItemDetails(title: "Olkeey", subtitle: "Organizing Events The Easy Way", image: URL(string: "https://dl.dropbox.com/s/pmggyp7j64nvvg8/Certificate%402x.png?dl=0")! , logo: #imageLiteral(resourceName: "Logo1"), color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), show: false)]


