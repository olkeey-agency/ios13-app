
//
//  RingVIew.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 20/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

var profit: Double = 90
var sales: Double = 100

struct RingView: View {
    var color1 = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    var color2 = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    var width: CGFloat = 88
    @Binding var show: Bool
   
  
    
    var body: some View {
        let coeficient = width / 44
        
        return ZStack {
            Circle().stroke(Color.black.opacity(0.1), style: StrokeStyle(lineWidth: 7 * coeficient))
                .frame(width: width, height: width)
            Circle()
                .trim(from: show ? CGFloat(Double(1.0) - profit / sales) : 1, to: 1)
                .stroke(LinearGradient(gradient: Gradient(colors: [Color(color1), Color(color2)]), startPoint:
                    .leading, endPoint: .trailing),
                        style: StrokeStyle(lineWidth: 5 * coeficient, lineCap: .round, lineJoin: .round, miterLimit: .infinity, dash: [20,0], dashPhase: 0))
                .rotationEffect(.degrees(90))
                .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
            .frame(width: width, height: width)
                .shadow(color: Color(color1).opacity(0.4), radius: 3 * coeficient, x: 0, y: 3 * coeficient)
            
            Text("\(Int(profit / sales * 100))%")
                .font(.system(size: 14 * coeficient * 0.80))
                .fontWeight(.semibold)
                .onAppear {
                    self.show.toggle()
            }
        }
    }
}

struct RingVIew_Previews: PreviewProvider {
    static var previews: some View {
        RingView(show: .constant(false))
    }
}
