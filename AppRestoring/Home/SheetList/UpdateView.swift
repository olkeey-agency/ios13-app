//
//  UpdateView.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 20/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

// UPDATE MODELS ARE RELATED TO THE LIST FROM THE SHEET VIEW

import SwiftUI

struct UpdateView: View {
    var update : Update = updateList[0]
    
    var body: some View {
        
        List {
            VStack {
                Image(update.image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: .infinity)
                Text(update.text)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            .navigationBarTitle(update.title)
        }
        .listStyle(PlainListStyle())
    }
}

struct UpdateView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateView()
    }
}

