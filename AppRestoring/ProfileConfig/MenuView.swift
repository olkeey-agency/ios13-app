//
//  MenuView.swift
//  CodeDesignStudies
//
//  Created by johnnie fujita on 19/02/20.
//  Copyright © 2020 johnnie fujita. All rights reserved.
//

import SwiftUI

struct MenuView: View {
    @State var showAccount = false
    @State var showCards = false
    var body: some View {
        VStack {
        
            Spacer()
            VStack(spacing: 16){
                Text("Johnnie - 40% completed")
                    .font(.caption)
                
                Color.white
                    .frame(width: 38, height: 6)
                    .cornerRadius(3)
                    .frame(width: 130, height: 6, alignment: .leading)
                    .background(Color.black.opacity(0.08))
                    .cornerRadius(3)
                    .frame(width: 150, height: 24)
                    .background(Color.black.opacity(0.1))
                    .cornerRadius(12)
                    
                
                Button(action: {
                    self.showAccount = true
                }) {
                    MenuRow(title: "Account", icon: "gear")
                    .foregroundColor(Color.black)
                }.sheet(isPresented: $showAccount) {
                        AccountConfig()
                }
                
                Button(action: {
                    self.showCards = true
                }) {
                    MenuRow(title: "Billing", icon: "creditcard")
                    .foregroundColor(Color.black)
                }.sheet(isPresented: $showCards) {
                    ContentView()
                }
                
                
                MenuRow(title: "Sign Out", icon: "person.crop.circle")
            }
            .frame(maxWidth: .infinity)
            .frame(height: 300)
            .background(LinearGradient(gradient: Gradient(colors: [Color("background3"), Color("background3").opacity(0.6)]), startPoint: .top, endPoint: .bottom))
            .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
            .shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 20)
            .padding(.horizontal, 30)
        .overlay(
            Image("Avatar")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 60, height: 60)
            .clipShape(Circle())
            .offset(y: -140)
            )
        }
        
        
        .padding(.bottom, 30)
    }
    
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}

struct MenuRow: View {
    var title: String
    var icon: String
    
    var body: some View {
        HStack(spacing: 16){
            Image(systemName: icon)
                .font(.system(size: 20, weight: .light))
                .imageScale(.large)
                .frame(width: 32, height: 32)
                .foregroundColor(Color(#colorLiteral(red: 0.662745098, green: 0.7333333333, blue: 0.831372549, alpha: 1)))
            Text(title)
                .font(.system(size: 20, weight: .bold))
                .frame(width: 120, alignment: .leading)
        }
    }
}

